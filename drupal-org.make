; Drupal.org release file.
core = 7.x
api = 2

; Include the definition for how to build Drupal core directly, including patches:
;includes[] = drupal-org-core.make

; Download the install profile and recursively build all its dependencies:
projects[commerce_kickstart][version] = 2.x-dev
projects[commerce_kickstart][patch][] = https://drupal.org/files/issues/2150303-open_commerce_kickstart-4.patch
